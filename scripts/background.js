'use strict';

chrome.browserAction.setBadgeBackgroundColor({ color: '#000' });

chrome.runtime.onInstalled.addListener(function (details) {
  console.log('previousVersion', details.previousVersion);
});

chrome.extension.onMessage.addListener(function(request, sender, sendResponse) {
  proxyRedirect(request.type, sendResponse);
  return true;
});

chrome.tabs.onUpdated.addListener(function (tabId, changeInfo, tab) {
  if (changeInfo.status == 'complete' && tab.active) {
    proxyRedirect('init_extension', function(result) {
      chrome.browserAction.setBadgeText({text: "" + result.value});
    });
    return true;
  }
});

chrome.tabs.onActivated.addListener(function (tabId, changeInfo, tab) {
  chrome.browserAction.setBadgeText({text: "0"});
  proxyRedirect('init_extension', function(result) {
    chrome.browserAction.setBadgeText({text: "" + result.value});
  });
});


//
// Send a message to the content script
// -----------------------------------------

var proxyRedirect = function(type, sendResponse) {
  chrome.tabs.getSelected(null, function(tab) {
    chrome.tabs.sendMessage(tab.id, {type: type}, sendResponse);
  });
}
