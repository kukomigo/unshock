'use strict';

chrome.extension.onMessage.addListener(function(message, sender, sendResponse) {
  switch(message.type) {
    case "init_extension":
      sendResponse({ value: initExtension(myFilters) });
    break;
    case "execute-szok-links":
      replaceTextInsideLinks(myFilters);
    break;
    case "execute-hide-links":
      hideUnwantedLinks(myFilters);
    break;
  }
});


//
// Helpers
// -----------------------------------------

function hasTagsInside(el) {
  var html = el.html();
  return html.match(/(\<|\>)/ig);
}

//
// Content methods
// -----------------------------------------

var initExtension = function(filters) {
  if (filters) {
    $('a').each(function() {
      var el = $(this);
      var str = el.html();
      $.each(filters, function(key, value) {
        if (str.match(value.regexp)) {
          el.addClass('extension-shock-link');
        }
      });
    });

    if ($('.extension-overlay').length === 0) {
      $('body').prepend('<div class="extension-overlay"></div>');
      $('.extension-overlay').css({
        background: '#fff',
        position: 'fixed',
        width: '100%',
        height: '100%',
        transition: 'all 0.6s linear',
        opacity: '0',
        top: '0',
        zIndex: '-1'
      });
    }

    return $('.extension-shock-link').length;
  }
};

var replaceTextInsideLinks = function(filters) {
  if (filters) {
    $('.extension-shock-link').each(function() {
      var el = $(this);
      if (hasTagsInside(el)) {
        $(el).find('span, strong, b').each(function() {
          var sub = $(this);
          $.each(filters, function(key, value) {
            var str = sub.html();
            sub.html(str.replace(value.regexp, value.result));
          });
        })
      } else {
        $.each(filters, function(key, value) {
          var str = el.text();
          el.text(str.replace(value.regexp, value.result));
        });
      }
    });
  }
};

var hideUnwantedLinks = function(filters) {
  if (+$('.extension-overlay').css('opacity') === 0) {
    $('*').css({ zIndex: 'auto' });
    $('.extension-shock-link').css({ 'z-index': '99999', position: 'relative' });
    $('.extension-shock-link').find('*').each(function() {
      $(this).css({ 'z-index': '99999' });
      var pos = $(this).css('position');
      if (pos !== 'relative' && pos !== 'absolute') {
        $(this).css({ position: 'relative' });
      }
    });
    $('.extension-overlay').css({ zIndex: '8888', opacity: '.9' });
  } else {
    $('.extension-overlay').css({ zIndex: '-1', opacity: '0' });
    setTimeout(function() {
      $('*').css({ zIndex: '' });
    }, 600);
  }
};