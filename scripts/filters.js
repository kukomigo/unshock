var myFilters = [
  {
    regexp: /szok(\s|!)/g,
    result: 'deficyt ciekawiści$1'
  },
  {
    regexp: /Szok(\s|!)/g,
    result: 'Deficyt ciekawiści$1'
  },
  {
    regexp: /szok(.*?)([a-z])(\s|!)/g,
    result: 'nudząc$2$3'
  },
  {
    regexp: /Szok(.*?)([a-z])(\s|!)/g,
    result: 'Nudząc$2$3'
  },
  {
    regexp: /Przerażając([a-z])(\s|!)/g,
    result: 'Zabawn$1$2'
  },
  {
    regexp: /przerażając([a-z])(\s|!)/g,
    result: 'zabawn$1$2'
  },
  {
    regexp: /\sstrasz(.*?)(\s|!)/ig,
    result: 'Bez sensu$2'
  },
  {
    regexp: /tragedia(\s|!)/g,
    result: 'komedia$1'
  },
  {
    regexp: /Tragedia(\s|!)/g,
    result: 'Komedia$1'
  },
  {
    regexp: /dramat(\s|!)/g,
    result: 'penis$1'
  },
  {
    regexp: /Dramat(\s|!)/g,
    result: 'Penis$1'
  }
];